# publish-drive-fulfillment

###env

export GOOGLE_APPLICATION_CREDENTIALS=/Users/holivera/Documents/gcp-functions/publish-drive/creds.json

###create the function
Note: dialogflowFirebaseFulfillment is the name of the function that we need to export in the index.js file.

* Configuration
PUBLISH_ENDPOINT = [my-publish-function-endpoint-url]

gcloud functions deploy dialogflowFirebaseFulfillment --runtime nodejs8 --trigger-http --memory 128MB  --set-env-vars PUBLISH_ENDPOINT=https://publish-drive-xyz.a.run.app
