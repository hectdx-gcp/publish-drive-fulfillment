
'use strict';
 
const functions = require('firebase-functions');
const {WebhookClient} = require('dialogflow-fulfillment');
const {Card, Suggestion} = require('dialogflow-fulfillment');
const requestPromise = require('request-promise');
 
process.env.DEBUG = 'dialogflow:debug'; // enables lib debugging statements

console.log("Publish Endpoint: " + process.env.PUBLISH_ENDPOINT);

const receivingServiceURL = process.env.PUBLISH_ENDPOINT;
 
exports.dialogflowFirebaseFulfillment = functions.https.onRequest((request, response) => {

  const agent = new WebhookClient({ request, response });
  
  function googleAssistantHandler(agent) {

    agent.add(`Publishing`);

    return new Promise((resolve, reject) => {
    
        authenticate().then((token) => {
           return callPublishFunction(token)
            .then((res) => {
                console.log("ok response", res);
                let out = agent.add("done");
                resolve(out);
            })
            .catch((error) => {
                console.error("error response", error);
                reject(error);
            });
        }).catch(err => {
            console.log("Unexpected error: " + err)
            reject(err);
        });

    });//promise

   }
  
  // Run the proper function handler based on the matched Dialogflow intent name
  let intentMap = new Map();
  intentMap.set('call service', googleAssistantHandler);
  agent.handleRequest(intentMap);
});

function callPublishFunction(token){

    if (!receivingServiceURL){
        throw 'Env var PUBLISH_ENDPOINT is not defined';
    }

    let reqOptions = {
        uri: receivingServiceURL
    };
    return requestPromise(reqOptions).auth(null, null, true, token);
}

function authenticate(){
    const metadataServerTokenURL = 'http://metadata/computeMetadata/v1/instance/service-accounts/default/identity?audience=';
    const tokenRequestOptions = {
        uri: metadataServerTokenURL + receivingServiceURL,
        headers: {
            'Metadata-Flavor': 'Google'
        }
    };
    return requestPromise(tokenRequestOptions);
}